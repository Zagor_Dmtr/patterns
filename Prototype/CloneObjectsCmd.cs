﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;

namespace Patterns.Prototype
{
    public class CloneObjectsCmd
    {
        [CommandMethod("Patterns_Prototype_CloneObjects")]
        public void RunCloneObjectsCmd()
        {
            Document adoc = Application.DocumentManager.MdiActiveDocument;
            Database db = adoc.Database;
            Editor ed = adoc.Editor;

            PromptSelectionResult selRes = ed.GetSelection();
            if (selRes.Status != PromptStatus.OK) return;

            PromptDoubleOptions scaleOpts
                = new PromptDoubleOptions("\nКоэффициент масштабирования:");
            scaleOpts.AllowNegative = false;
            scaleOpts.AllowZero = false;
            scaleOpts.DefaultValue = 1.0;
            PromptDoubleResult scaleRes = ed.GetDouble(scaleOpts);
            if (scaleRes.Status != PromptStatus.OK) return;

            PromptPointResult basePtRes
                = ed.GetPoint("\nБазовая точка:");
            if (basePtRes.Status != PromptStatus.OK) return;

            PromptPointOptions newBasePtOpt
                = new PromptPointOptions("\nНовое положение базовой точки:");

            newBasePtOpt.BasePoint = basePtRes.Value;
            newBasePtOpt.UseBasePoint = true;            
            PromptPointResult newBasePtRes = ed.GetPoint(newBasePtOpt);
            if (newBasePtRes.Status != PromptStatus.OK) return;

            Matrix3d ucs = ed.CurrentUserCoordinateSystem;
            Matrix3d scale = Matrix3d.Scaling
                (scaleRes.Value, basePtRes.Value.TransformBy(ucs));
            Matrix3d move = Matrix3d.Displacement
                (basePtRes.Value.TransformBy(ucs).GetVectorTo
                (newBasePtRes.Value.TransformBy(ucs)));
            
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                BlockTableRecord curSpace = tr.GetObject
                    (db.CurrentSpaceId, OpenMode.ForWrite) as BlockTableRecord;
                foreach (ObjectId id in selRes.Value.GetObjectIds())
                {
                    Entity entity = tr.GetObject
                        (id, OpenMode.ForRead, false, true) as Entity;

                    /* В качестве прототипов выступают выбранные объекты.
                     * Они имеют базовый метод Clone который позволяет
                     * создать копию объекта не заботясь о типе объекта,
                     * его геометрии, свойствах и т.п.
                     */
                    Entity clone = entity.Clone() as Entity;
                    clone.TransformBy(scale);
                    clone.TransformBy(move);
                    curSpace.AppendEntity(clone);
                    tr.AddNewlyCreatedDBObject(clone, true);
                }
                tr.Commit();
            }
        }
    }
}
