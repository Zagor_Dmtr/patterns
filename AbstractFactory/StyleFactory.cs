﻿namespace Patterns.AbstractFactory
{
    internal abstract class StyleFactory
    {
        public abstract ColorData CreateColorData();

        public abstract LineWeightData CreateLineWeightData();
    }
}
