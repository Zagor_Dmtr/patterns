﻿using Autodesk.AutoCAD.DatabaseServices;

namespace Patterns.AbstractFactory.LineWeights
{
    internal class ConstLineWeight : LineWeightData
    {
        private readonly LineWeight _lineWeight;

        public ConstLineWeight(LineWeight lineWeight)
        {
            _lineWeight = lineWeight;
        }

        public override LineWeight GetLineWeight() => _lineWeight;
    }
}
