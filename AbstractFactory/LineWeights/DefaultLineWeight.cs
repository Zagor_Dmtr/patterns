﻿using Autodesk.AutoCAD.DatabaseServices;

namespace Patterns.AbstractFactory.LineWeights
{
    internal class DefaultLineWeight : LineWeightData
    {
        private readonly LineWeight _lineWeight;

        public DefaultLineWeight(Database db)
        {
            _lineWeight = db.Celweight;
        }

        public override LineWeight GetLineWeight() => _lineWeight;
    }
}
