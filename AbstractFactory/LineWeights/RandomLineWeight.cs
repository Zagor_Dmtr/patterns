﻿using Autodesk.AutoCAD.DatabaseServices;
using System;
using System.Linq;

namespace Patterns.AbstractFactory.LineWeights
{
    internal class RandomLineWeight : LineWeightData
    {
        public override LineWeight GetLineWeight()
        {
            int index = Support.GetRandom(0, 27);
            return Enum
                .GetValues(typeof(LineWeight))
                .Cast<LineWeight>()
                .ElementAtOrDefault(index);
        }
    }
}
