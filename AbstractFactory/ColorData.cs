﻿using Autodesk.AutoCAD.Colors;

namespace Patterns.AbstractFactory
{
    internal abstract class ColorData
    {
        public abstract Color GetColor();
    }
}
