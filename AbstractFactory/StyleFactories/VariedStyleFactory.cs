﻿using Patterns.AbstractFactory.Colors;
using Patterns.AbstractFactory.LineWeights;

namespace Patterns.AbstractFactory.StyleFactories
{
    internal class VariedStyleFactory : StyleFactory
    {
        public override ColorData CreateColorData() => new RandomRGBColor();

        public override LineWeightData CreateLineWeightData() => new RandomLineWeight();
    }
}
