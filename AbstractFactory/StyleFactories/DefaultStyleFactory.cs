﻿using Autodesk.AutoCAD.DatabaseServices;
using Patterns.AbstractFactory.Colors;
using Patterns.AbstractFactory.LineWeights;

namespace Patterns.AbstractFactory.StyleFactories
{
    internal class DefaultStyleFactory : StyleFactory
    {
        public override ColorData CreateColorData()
        {
            Database db = HostApplicationServices.WorkingDatabase;
            return new DefaultColor(db);
        }


        public override LineWeightData CreateLineWeightData()
        {
            Database db = HostApplicationServices.WorkingDatabase;
            return new DefaultLineWeight(db);
        }
    }
}
