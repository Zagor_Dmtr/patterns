﻿using Autodesk.AutoCAD.DatabaseServices;
using Patterns.AbstractFactory.Colors;
using Patterns.AbstractFactory.LineWeights;

namespace Patterns.AbstractFactory.StyleFactories
{
    internal class FixedStyleFactory : StyleFactory
    {
        private readonly int _index;
        private readonly LineWeight _lineWeight;

        public FixedStyleFactory(int index, LineWeight lineWeight)
        {
            _index = index;
            _lineWeight = lineWeight;
        }

        public override ColorData CreateColorData()
            => new ConstColor(_index);

        public override LineWeightData CreateLineWeightData()
            => new ConstLineWeight(_lineWeight);
    }
}
