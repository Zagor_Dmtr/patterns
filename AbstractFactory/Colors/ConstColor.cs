﻿using Autodesk.AutoCAD.Colors;

namespace Patterns.AbstractFactory.Colors
{
    internal class ConstColor : ColorData
    {
        private readonly short _index;

        public ConstColor(int index)
        {
            _index = (short)index;
        }

        public override Color GetColor()
            => Color.FromColorIndex
            (ColorMethod.ByAci, _index);
    }
}
