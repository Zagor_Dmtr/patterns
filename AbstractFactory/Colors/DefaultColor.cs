﻿using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;

namespace Patterns.AbstractFactory.Colors
{
    internal class DefaultColor : ColorData
    {
        private readonly Color _color;

        public DefaultColor(Database db)
        {
            _color = db.Cecolor;
        }

        public override Color GetColor() => _color;
    }
}
