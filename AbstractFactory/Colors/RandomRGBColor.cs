﻿using Autodesk.AutoCAD.Colors;

namespace Patterns.AbstractFactory.Colors
{
    internal class RandomRGBColor : ColorData
    {
        public override Color GetColor()
        {
            byte GetRandomByte()
            {
                int value = Support.GetRandom(0, 256);
                return (byte)value;
            }

            byte
                red = GetRandomByte(),
                green = GetRandomByte(),
                blue = GetRandomByte();

           return Color.FromRgb(red, green, blue);
        }
    }
}
