﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
using Patterns.AbstractFactory.StyleFactories;

namespace Patterns.AbstractFactory
{
    public class SetStyleCmd
    {
        [CommandMethod("Patterns_AbstractFactory_SetStyle")]
        public void RunSetStyleCmd()
        {
            Document adoc
                = Application.DocumentManager.MdiActiveDocument;
            Database db = adoc.Database;
            Editor ed = adoc.Editor;

            PromptSelectionResult selRes = ed.GetSelection();
            if (selRes.Status != PromptStatus.OK) return;            

            PromptKeywordOptions keyOpts
            = new PromptKeywordOptions("\nЗадать стиль: ");
            keyOpts.Keywords.Add("Fixed", "Постоянный");
            keyOpts.Keywords.Add("Varied", "Вариативный");
            keyOpts.Keywords.Add("Default", "Сброс");
            PromptResult keyRes = ed.GetKeywords(keyOpts);
            if (keyRes.Status != PromptStatus.OK) return;

            StyleFactory factory;
            switch (keyRes.StringResult)
            {
                case "Fixed":
                    factory = new FixedStyleFactory
                        (1, LineWeight.LineWeight100);
                    break;
                case "Varied":
                    factory = new VariedStyleFactory();
                    break;
                default:
                    factory = new DefaultStyleFactory();
                    break;
            }

            Style style = new Style(factory);

            foreach (ObjectId id in selRes.Value.GetObjectIds())
            {
                using (Entity entity = id.SafeOpen<Entity>(true))
                {
                    if (entity != null)
                    {
                        style.ApplyStyle(entity);                        
                    }
                }
            }

            db.LineWeightDisplay = true;
        }
    }
}
