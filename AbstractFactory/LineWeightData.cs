﻿using Autodesk.AutoCAD.DatabaseServices;

namespace Patterns.AbstractFactory
{
    internal abstract class LineWeightData
    {
        public abstract LineWeight GetLineWeight();
    }
}
