﻿using Autodesk.AutoCAD.DatabaseServices;

namespace Patterns.AbstractFactory
{
    internal class Style
    {
        private readonly ColorData _color;
        private readonly LineWeightData _lineWeight;

        public Style(StyleFactory style)
        {
            _color = style.CreateColorData();
            _lineWeight = style.CreateLineWeightData();
        }

        public void SetColor(Entity entity)
        {
            entity.Color = _color.GetColor();
        }

        public void SetLineWeight(Entity entity)
        {
            entity.LineWeight = _lineWeight.GetLineWeight();
        }

        public void ApplyStyle(Entity entity)
        {
            SetColor(entity);
            SetLineWeight(entity);
        }
    }
}
