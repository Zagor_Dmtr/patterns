﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;

namespace Patterns.Singleton
{
    /// <summary>
    /// Часто встречающаяся в кодах для AutoCAD конструкция
    /// класса-одиночки для пользовательских переопределений.
    /// В данном случае переопределяется отрисовка линий -
    /// добавляются две окружности на концах линии.
    /// </summary>
    internal class CustomDrawOverrule : DrawableOverrule
    {
        /// <summary>
        /// Статическая переменная для хранения
        /// едиственного экземпляра данного класса
        /// </summary>
        private static CustomDrawOverrule _instance;        

        /// <summary>
        /// Метод для доступа к единственному экземпляру
        /// класса. Один из вариантов использования отложенной
        /// инициализации - объект создаётся при первом обращении
        /// к нему.
        /// </summary>
        private static CustomDrawOverrule GetInstance()
            => _instance ?? (_instance = new CustomDrawOverrule());

        /// <summary>
        /// Единственный конструктор закрыт внутри класса.
        /// То есть, извне объект не получится создать (если
        /// не использовать рефлексию)
        /// </summary>
        private CustomDrawOverrule() { }

        /// <summary>
        /// Флаг для проверки включенности перерисовки
        /// </summary>
        public static bool IsOn { get; private set; }

        /// <summary>
        /// Метод для изменения отрисовки линий в модели
        /// </summary>
        /// <param name="drawable"></param>
        /// <param name="wd"></param>
        /// <returns></returns>
        public override bool WorldDraw(Drawable drawable, WorldDraw wd)
        {
            if (drawable is Line line
                // Перерисовываем только линии в БД чертежа                
                && line.Database != null
                // ... которые находятся в текущем пространстве
                && line.BlockId.Equals(line.Database.CurrentSpaceId))
            {
                double radius = line.Length / 8.0;
                wd.Geometry.Circle
                    (line.StartPoint, radius, Vector3d.ZAxis);
                wd.Geometry.Circle
                    (line.EndPoint, radius, Vector3d.ZAxis);
            }

            return base.WorldDraw(drawable, wd);
        }

        /// <summary>
        /// Метод для включения-отключения отрисовки
        /// </summary>
        public static void OnOffOverrule()
        {
            // Проверяем флаг. Если перерисовка не включена
            if (!IsOn)
            {
                // Включаем перерисовку
                AddOverrule(GetClass(typeof(Line)), GetInstance(), false);

                // На всякий случай - включаем
                // перерисовку глобально
                Overruling = true;

                // Задаём новое значение флагу
                IsOn = true;
            }
            // Если перерисовка включена
            else
            {
                // Отключаем перерисовку
                RemoveOverrule(GetClass(typeof(Line)), GetInstance());

                // Задаём новое значение флагу
                IsOn = false;
            }
        }
    }
}
