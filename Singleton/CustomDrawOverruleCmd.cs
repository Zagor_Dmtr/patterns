﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;

namespace Patterns.Singleton
{
    public class CustomDrawOverruleCmd
    {
        [CommandMethod("Patterns_Singleton_RunStopOverrule")]
        public void RunStopOverrule()
        {            
            CustomDrawOverrule.OnOffOverrule();

            Editor ed = Application
                .DocumentManager.MdiActiveDocument.Editor;

            // Выводим подсказку о текущем состоянии
            ed.WriteMessage
                ("\nПерерисовка линий {0}!\n",
                CustomDrawOverrule.IsOn ? "включена" : "отключена");

            // Обновляем отрисовку объектов чертежа
            // чтобы сразу увидеть новую отрисовку
            ed.Regen();
        }
    }
}
