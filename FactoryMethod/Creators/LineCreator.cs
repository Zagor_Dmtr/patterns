﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace Patterns.FactoryMethod.ConcreteCreators
{
    internal sealed class LineCreator : EntityCreator
    {
        private readonly Point3d _startPoint, _endPoint;

        public LineCreator(Point3d startPoint, Point3d endPoint)
        {
            _startPoint = startPoint;
            _endPoint = endPoint;
        }

        public LineCreator GetMirrored()
        {
            Vector3d horOffset = Vector3d.XAxis
                .MultiplyBy(_endPoint.X - _startPoint.X);

            Point3d
                startPt = _startPoint + horOffset,
                endPt = _endPoint - horOffset;

            return new LineCreator(startPt, endPt);
        }

        public override Entity CreateEntity()
            => new Line(_startPoint, _endPoint);
    }
}
