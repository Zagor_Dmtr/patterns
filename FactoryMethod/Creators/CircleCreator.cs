﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace Patterns.FactoryMethod.ConcreteCreators
{
    internal sealed class CircleCreator : EntityCreator
    {
        private readonly Point3d _center;
        private readonly double _radius;

        public CircleCreator(Point3d firstPoint, Point3d secondPoint)
        {
            _center = Support.GetMidPoint
                (firstPoint, secondPoint,
                out Vector3d diagonal);
            _radius = diagonal.Length / 2;
        }

        public override Entity CreateEntity()
            => new Circle(_center, Vector3d.ZAxis, _radius);
    }
}
