﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace Patterns.FactoryMethod.ConcreteCreators
{
    internal sealed class RectangleCreator : FigureCreator
    {
        public RectangleCreator(Point3d firstPoint, Point3d secondPoint)
            : base(firstPoint, secondPoint)
        {            
        }

        public override Entity CreateEntity()
        {
            Polyline pline = new Polyline(4);
            pline.AddVertexAt(0, _center + _horOffset - _vertOffset, 0, 0, 0);
            pline.AddVertexAt(1, _center + _horOffset + _vertOffset, 0, 0, 0);
            pline.AddVertexAt(2, _center - _horOffset + _vertOffset, 0, 0, 0);
            pline.AddVertexAt(3, _center - _horOffset - _vertOffset, 0, 0, 0);
            pline.Closed = true;
            return pline;
        }
    }
}
