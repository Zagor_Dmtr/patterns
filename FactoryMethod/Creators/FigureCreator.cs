﻿using Autodesk.AutoCAD.Geometry;
using System;

namespace Patterns.FactoryMethod
{
    internal abstract class FigureCreator : EntityCreator
    {
        protected readonly Point2d _center;
        protected readonly Vector2d _horOffset, _vertOffset;

        public FigureCreator(Point3d firstPoint, Point3d secondPoint)
        {
            _center = Support.GetMidPoint
                (firstPoint, secondPoint,
                out Vector3d diagonal)
                .Convert2d(Support.PlaneXY);

            double
                width = Math.Abs(diagonal.X) / 2,
                height = Math.Abs(diagonal.Y) / 2;

            _horOffset = Vector2d.XAxis.MultiplyBy(width);
            _vertOffset = Vector2d.YAxis.MultiplyBy(height);
        }
    }
}
