﻿using Autodesk.AutoCAD.DatabaseServices;

namespace Patterns.FactoryMethod
{
    internal abstract class EntityCreator
    {        
        public abstract Entity CreateEntity();
    }
}
