﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using Patterns.FactoryMethod.ConcreteCreators;
using System.Collections.Generic;

namespace Patterns.FactoryMethod
{
    public class CreateEntitiesCmd
    {
        [CommandMethod("Patterns_FactoryMethod_CreateEntities")]
        public void RunCreateEntitiesCmd()
        {
            Document adoc = Application.DocumentManager.MdiActiveDocument;
            Database db = adoc.Database;
            Editor ed = adoc.Editor;

            PromptPointResult firstPtRes
                = ed.GetPoint("\nПерая точка главной диагонали: ");
            if (firstPtRes.Status != PromptStatus.OK) return;
            
            PromptPointResult secondPtRes = ed.GetCorner
                ("\nВторая точка главной диагонали: ", firstPtRes.Value);
            if (secondPtRes.Status != PromptStatus.OK) return;

            Matrix3d currentUCS = ed.CurrentUserCoordinateSystem;

            Point3d
                firstPt = firstPtRes.Value.TransformBy(currentUCS),
                secondPt = secondPtRes.Value.TransformBy(currentUCS);

            List<EntityCreator> creators = new List<EntityCreator>();
            LineCreator firstLine = new LineCreator(firstPt, secondPt);
            creators.Add(firstLine);
            creators.Add(firstLine.GetMirrored());
            creators.Add(new CircleCreator(firstPt, secondPt));
            creators.Add(new RectangleCreator(firstPt, secondPt));
            creators.Add(new RhombusCreator(firstPt, secondPt));

            using (BlockTableRecord curSpace
                = db.CurrentSpaceId.SafeOpen<BlockTableRecord>(true))
            {
                foreach (EntityCreator creator in creators)
                {
                    using (Entity entity = creator.CreateEntity())
                    {
                        curSpace?.AppendEntity(entity);
                    }
                }
            }
        }
    }
}
