﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using System;
using System.Diagnostics;

namespace Patterns
{
    static class Support
    {
        private static Random random = new Random();

        public static int GetRandom
            (int max, int min) => random.Next(max, min);

        public static Plane PlaneXY
            = new Plane(Point3d.Origin, Vector3d.ZAxis);

        public static Point3d GetMidPoint
            (Point3d point1, Point3d point2, out Vector3d fromPt1ToPt2)
        {
            fromPt1ToPt2 = point1.GetVectorTo(point2);
            return point1 + fromPt1ToPt2.DivideBy(2);
        }
               
        public static T SafeOpen<T>(this ObjectId id, OpenMode mode,
            bool openErased = false,
            bool forceOpenOnLockedLayer = true) where T : DBObject
        {
            T obj = null;

            if (id.CheckObjectIdFor<T>(openErased))
            {
                try
                {
#pragma warning disable 618
                    obj = id.Open(mode, openErased, forceOpenOnLockedLayer) as T;
#pragma warning restore 618                    
                }
                catch (System.Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    Debug.WriteLine(ex.StackTrace);
                }
            }

            return obj;
        }

        public static T SafeOpen<T>(this ObjectId id,
            bool forWrite = false,
            bool openErased = false,
            bool forceOpenOnLockedLayer = true) where T : DBObject
            => SafeOpen<T>
                (id,
                forWrite ? OpenMode.ForWrite : OpenMode.ForRead,
                openErased,
                forceOpenOnLockedLayer);

        public static bool CheckObjectIdFor<T>
           (this ObjectId id,
           bool allowErased = false) where T : DBObject
        {
            RXClass forCheck = RXObject.GetClass(typeof(T));
            return !id.IsNull
                && id.IsValid
                && (allowErased
                    || (!id.IsErased && !id.IsEffectivelyErased))
                && (id.ObjectClass.Equals(forCheck)
                    || id.ObjectClass.IsDerivedFrom(forCheck));
        }
    }
}
